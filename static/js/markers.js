var sydneyLayer;

function highlightFeature(e) {
  var layer = e.target;

  layer.setStyle({
    weight : 5,
    color : 'orange',
    fillColor : 'white',
    fillOpacity : 0.2
  });

  if(!L.Browser.ie && !L.Browser.opera) {
    layer.bringToFront();
  }

}

function resetHighlight(e) {
  sydneyLayer.resetStyle(e.target);
}

function zoomToFeature(e) {
  map.fitBounds(e.target.getBounds());
}

var markers = new Array();

function SydneyOnEachFeature(feature, layer) {
  layer.bindLabel(feature.properties.name.toString(), {noHide : true});

  markers.push(
    L.circleMarker(
      layer.getBounds().getCenter(),
      {
        radius : 0.0,
        opacity : 0,
        fillOpacity : 0
      }
    )
  );

  layer.on(
    {
      mouseover : highlightFeature,
      mouseout  : resetHighlight,
      click : zoomToFeature
    }
  );

}


function getSydneyColor(Pop)
{

  if(Pop > 10000) {

    return 'red';

  } else if (Pop > 5000) {

    return 'blue';

  }else if (Pop < 5000){

    return 'green';
  }

}

function sydneyStyle(feature)
{
  return{
    fillColor : getSydneyColor(feature.properties.population),
    weight : 1,
    opacity : 0.01,
    color : 'white',
    dashArray : 3,
    fillOpacity : 0.2
  }
}
//
var southWest = L.latLng(-33.879290, 151.043805),
northEast = L.latLng(-33.886050, 151.053815),
bounds = L.latLngBounds(southWest, northEast);


//            var map = L.map('map',{scrollWheelZoom:false}).setView([-33.881329, 151.048772], 16.45);

var map = L.map('map', {

  zoom : 0,
  minZoom : 16,
  maxZoom : 30,
  maxBounds : bounds


});


marks = new Array();

map.fitBounds(bounds);
map.setMaxBounds(bounds);
//
var googleRoadMapLayer = new L.Google('ROADMAP');

map.addLayer(googleRoadMapLayer);

var googleLayer = new L.Google();

map.addLayer(googleRoadMapLayer);

var point = [-33.880465, 151.049952];

var myMarker = L.marker(point, {title: "myMarker"});


//            myMarker.addTo(map);

myMarker.bindPopup(

  '<b> East St opp University of Sydney Cumberland Campus, Gate 1 </b><div><img style="width : 100%" src="data/images/gate.jpg" alt="image" /></div>',

  {minWidth : 200}

);

// create custom icon
var hockeyIcon = L.icon({
  iconUrl: 'images/hockey_icon.png',
  iconSize: [60,65], // size of the icon
});

var point2 = [-33.884269, 151.049057];

var myMarker2 = L.marker(point2, {icon:hockeyIcon});

//            myMarker2.addTo(map);

myMarker2.bindPopup(

  '<b> Sydney University Hockey Club  </b><div><img style="width : 100%" src="data/images/usydhockey.jpg" alt="image" /></div>',

  {minWidth : 200}

);

// create custom icon
var foodIcon = L.icon({
  iconUrl: 'images/food_icon.png',
  iconSize: [38,38], // size of the icon
});

var point3 = [-33.881464, 151.048710];

var myMarker3 = L.marker(point3,  {icon: foodIcon});

//            myMarker3.addTo(map);
myMarker3.bindPopup(

  '<b> Building F The Depot-Cumberland Student Guild </b><a onclick="openNav(\'blockf\')"><div><img style="width : 100%" src="data/images/building.png" alt="image" /></a></div>',

  {minWidth : 200}

);

var gymIcon = L.icon({
  iconUrl: 'images/sport_icon.png',
  iconSize: [45,45], // size of the icon
//  shadowUrl: "lib/leaflet/images/marker-shadow.png",
//  shadowAnchor: [0, 10],
});

var point4 = [-33.883555, 151.050260];

var myMarker4 = L.marker(point4, {icon:gymIcon});

myMarker4.bindPopup(

  '<b> Building V Sport Centre</b><div><a onclick="openNav(\'blockv\')"><img style="width : 100%" src="data/images/building.png" alt="image" /></a></div>',

  {minWidth : 200}

);

var tennisIcon = L.icon({
  iconUrl: 'images/tennis1_icon.png',
  iconSize: [55,55], // size of the icon

});

var point5 = [-33.883298, 151.049213];

var myMarker5 = L.marker(point5, {icon:tennisIcon});

myMarker5.bindPopup(

  '<b> Tennis Pavillion </b><div><img style="width : 100%" src="data/images/building.png" alt="image" /></div>',

  {minWidth : 200}

);

var point6 = [-33.881573, 151.049115];

var myMarker6 = L.marker(point6);

myMarker6.bindPopup(

  '<b> SDN Ngallia Children Services </b><div><img style="width : 100%" src="data/images/Cumberland buildings/SDN Ngallia2.png" alt="image" /></div>',

  {minWidth : 200}

);

var point7 = [-33.881760, 151.048631];

var myMarker7 = L.marker(point7);

myMarker7.bindPopup(

  '<b> Building R Health Sciences Library </b><div><a onclick="openNav(\'blockr\')"><img style="width : 100%" src="data/images/Cumberland buildings/Libary2.png" alt="image" /><a/></div>',

  {minWidth : 200}

);

var templeIcon = L.icon({
  iconUrl: 'images/temple_icon.png',
  iconSize: [43,43], // size of the icon

});

var point8 = [-33.881165, 151.047764];

var myMarker8 = L.marker(point8, {icon:templeIcon});


myMarker8.bindPopup(

  '<b> Block P Prayer Room </b><a onclick="openNav(\'blockp\')"><div><img style="width : 100%" src="data/images/Cumberland buildings/P1000971- prayer room.JPG" alt="image" /><a/></div>',

  {minWidth : 200}

);

var point9 = [-33.881247, 151.049084];

var myMarker9 = L.marker(point9, {icon: foodIcon});

myMarker9.bindPopup(

  '<b>JDV Food Court</b><div><a onclick="openNav(\'blockfood\')"><img style="width : 100%" src="images/burger.jpg" alt="image" /><a/></div>',

  {minWidth : 200}

);

var point10 = [-33.881999, 151.049462];

var myMarker10 = L.marker(point10);

myMarker10.bindPopup(

  '<b> Building A Jeffrey Miller Administration </b><div><a onclick="openNav(\'blockadmin\')"><img style="width : 100%" src="data/images/building.png" alt="image" /><a/></div>',

  {minWidth : 200}

);

var point11 = [-33.881254, 151.048311];

var myMarker11 = L.marker(point11);

myMarker11.bindPopup(

  '<b>Building B</b><div><a onclick="openNav(\'blockb\')"><img style="width : 100%" src="data/images/building.png" alt="image" /><a/></div>',

  {minWidth : 200}

);

var point12 = [-33.882432, 151.049694];

var myMarker12 = L.marker(point12);

myMarker12.bindPopup(

  '<b>Building E</b><div><a onclick="openNav(\'blocke\')"><img style="width : 100%" src="data/images/building.png" alt="image" /><a/></div>',

  {minWidth : 200}

);

var point13 = [-33.882793, 151.049528];

var myMarker13 = L.marker(point13);

myMarker13.bindPopup(

  '<b> Building K Sport Science and Exercise Physiology </b><div><a onclick="openNav(\'blockk\')"><img style="width : 100%" src="data/images/building.png" alt="image" /><a/></div>',

  {minWidth : 200}

);

var point14 = [-33.882076, 151.048331];

var myMarker14 = L.marker(point14);

myMarker14.bindPopup(

  '<b> Block L Anatomy Labs </b><div><a onclick="openNav(\'blockl\')"><img style="width : 100%" src="data/images/building.png" alt="image" /><a/></div>',

  {minWidth : 200}

);

var point15 = [-33.882521, 151.047569];

var myMarker15 = L.marker(point15);

myMarker15.bindPopup(

  '<b> Building M MRS Administration </b><div><a onclick="openNav(\'blockm\')"><img style="width : 100%" src="data/images/building.png" alt="image" /><a/></div>',

  {minWidth : 200}

);

var point16 = [-33.882824, 151.047794];

var myMarker16 = L.marker(point16);

myMarker16.bindPopup(

  '<b> Building N </b><div><a onclick="openNav(\'blockn\')"><img style="width : 100%" src="data/images/building.png" alt="image" /><a/></div>',

  {minWidth : 200}

);

var point17 = [-33.882900, 151.048273];

var myMarker17 = L.marker(point17);

myMarker17.bindPopup(

  '<b> Building O Physiotherapy </b><div><a onclick="openNav(\'blocko\')"><img style="width : 100%" src="data/images/building.png" alt="image" /><a/></div>',

  {minWidth : 200}

);

var point18 = [-33.882561, 151.048477];

var myMarker18 = L.marker(point18);

myMarker18.bindPopup(

  '<b> Building S Speech Pathology </b><div><a onclick="openNav(\'blocks\')"><img style="width : 100%" src="data/images/building.png" alt="image" /><a/></div>',

  {minWidth : 200}

);

var point19 = [-33.880364, 151.048812];

var myMarker19 = L.marker(point19);

myMarker19.bindPopup(

  '<b> Building T Rehabilitation and Counselling </b><div><a onclick="openNav(\'blockt\')"><img style="width : 100%" src="data/images/building.png" alt="image" /><a/></div>',

  {minWidth : 200}

);

var busIcon = L.icon({
  iconUrl: 'images/busstop.png',
  iconSize: [43,43], // size of the icon

});

var point20 = [-33.881407, 151.050125];

var myMarker20 = L.marker(point20, {icon:busIcon});

myMarker20.bindPopup(

  '<b>Bus Stop, East St Gate 2</b><div></div>',

  {minWidth : 100}

);



var visible;

map.on(

  'zoomend',
  function(e)
  {
    if(map.getZoom() > 17)
    {
      if(!visible)
      {
        for(var i = 0; i < markers.length; i++)
        {
          markers[i].showLabel();
        }

        visible = true;
      }

    }else{

      if(visible)
      {
        for(var i = 0; i < markers.length; i++)
        {
          markers[i].HideLabel();
        }

        visible = false;
      }


    }

  }


);

var legend = L.control({position : 'bottomright'});

legend.onAdd = function(map)
{
  var div = L.DomUtil.create('div', 'legend');

  var labels = ["Population greater than 10000",                  "Population greater than 5000",
  "Population less than 5000"];

  var grades = [10001, 5001, 4999];

  div.innerHTML = '<div><b> Legend </b></div>';

  for(var i = 0; i < grades.length; i++)
  {
    div.innerHTML += '<i style="background:' + getSydneyColor(grades[i]) + '">&nbsp;&nbsp; </i>&nbsp;&nbsp;' + labels[i] + '<br />'
  }

  return div;
}
//            legend.addTo(map);


var baseMaps = {
  'Google RoadMap' : googleRoadMapLayer,
  'Google ' : googleLayer
};

var overlayMaps = {

  'Building A Jeffrey Miller Administration' : myMarker10,
  'Campus Gate'  : myMarker,
  'Hockey Club'  : myMarker2,
  'Depot-Cumberland Student Guild'  : myMarker3,
  'Building V Sport Centre'  : myMarker4,
  'Tennis Pavillion ' : myMarker5,
  'SDN Ngallia Children Services'  : myMarker6,
  'Building R Health Sciences Library'  : myMarker7,
  'Prayer Room Complex'  : myMarker8,
  'JDV Food Court'  : myMarker9,
  'Building B'  : myMarker11,
  'Building E'  : myMarker12,
  'Building K Sport Science/Exercise Physiology'  : myMarker13,
  'Block L Anatomy Labs': myMarker14,
  'Building M MRS Administration' : myMarker15,
  'Building N'  : myMarker16,
  'Building O Physiotherapy' : myMarker17,
  'Building S Speech Pathology'  : myMarker18,
  'Building T Rehabilitation and Counselling' : myMarker19,
  'Bus Stop'  : myMarker20

};


var food = L.layerGroup([myMarker9, myMarker3]);
var sport = L.layerGroup([myMarker2, myMarker4, myMarker5]);
var buildings = L.layerGroup([myMarker4, myMarker6, myMarker7, myMarker8]);

L.control.layers(baseMaps,overlayMaps).addTo(map);


//google.maps.event.addListener(myMarker4, 'click', function() {window.location.href = myMarker4.url;});
/* Open the sidenav */

function openNav(name) {
  if(name == 'right-panel'){
    document.getElementById(name).style.width = "100%";
  } else {
    document.getElementById(name).style.width = "100%";
  }

}
/* Close/hide the sidenav */
function closeNav(name) {
  document.getElementById(name).style.width = "0";
}

function initMapTransport() {
  var directionsService = new google.maps.DirectionsService();
  var directionsDisplay = new google.maps.DirectionsRenderer();
  var usyd = new google.maps.LatLng(-33.872951, 151.046501);
  var mapOptions = {
    zoom:15,
    center: usyd
  }

  var map = new google.maps.Map(document.getElementById('map'), mapOptions);
  directionsDisplay.setMap(map);
  directionsDisplay.setPanel(document.getElementById('right-panel'));
  calculateRoute(directionsService, directionsDisplay) ;

}

function initMap() {
  var southWest = L.latLng(-33.879290, 151.043805),
  northEast = L.latLng(-33.886050, 151.053815),
  bounds = L.latLngBounds(southWest, northEast);

  var mapOption = {
    zoom : 0,
    minZoom : 16,
    maxZoom : 30,
    maxBounds : bounds
  }

  map = new google.maps.Map(document.getElementById('map'),mapOption);

}

function calculateRoute(directionsService, directionsDisplay) {
  directionsService.route({
    origin:{lat:-33.863345, lng:151.045118},
    destination: {lat:-33.881331, lng:151.050290},
    travelMode: 'TRANSIT'
  }, function(response, status) {
    if (status === 'OK') {
      directionsDisplay.setDirections(response);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

function pop(){
  myMarker8.openPopup();
}

$(document).ready(function(){
  $("a").click(function(){
    pop($(this)[0].id);
  });
});




$("#food").click(function(event) {
  console.log(event);
  event.preventDefault();
  if(map.hasLayer(food)) {
    $(this).removeClass('selected');
    map.removeLayer(food);
  } else {
    map.addLayer(food);
    $(this).addClass('selected');
  }
});

$("#sport").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(sport)) {
    $(this).removeClass('selected');
    map.removeLayer(sport);
  } else {
    map.addLayer(sport);
    $(this).addClass('selected');
  }
});

$("#buildings").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(buildings)) {
    $(this).removeClass('selected');
    map.removeLayer(buildings);
  } else {
    map.addLayer(buildings);
    $(this).addClass('selected');
  }
});

$("#bus").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker20)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker20);
  } else {
    map.addLayer(myMarker20);
    $(this).addClass('selected');
  }
});

$("#block_a").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker10)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker10);
  } else {
    map.addLayer(myMarker10);
    $(this).addClass('selected');
  }
});

$("#block_b").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker11)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker11);
  } else {
    map.addLayer(myMarker11);
    $(this).addClass('selected');
  }
});

$("#block_e").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker12)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker12);
  } else {
    map.addLayer(myMarker12);
    $(this).addClass('selected');
  }
});

$("#block_k").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker13)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker13);
  } else {
    map.addLayer(myMarker13);
    $(this).addClass('selected');
  }
});

$("#block_l").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker14)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker14);
  } else {
    map.addLayer(myMarker14);
    $(this).addClass('selected');
  }
});

$("#block_m").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker15)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker15);
  } else {
    map.addLayer(myMarker15);
    $(this).addClass('selected');
  }
});

$("#block_n").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker16)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker16);
  } else {
    map.addLayer(myMarker16);
    $(this).addClass('selected');
  }
});

$("#block_o").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker17)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker17);
  } else {
    map.addLayer(myMarker17);
    $(this).addClass('selected');
  }
});

$("#block_p").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker8)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker8);
  } else {
    map.addLayer(myMarker8);
    $(this).addClass('selected');
  }
});

$("#block_r").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker7)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker7);
  } else {
    map.addLayer(myMarker7);
    $(this).addClass('selected');
  }
});

$("#block_s").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker18)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker18);
  } else {
    map.addLayer(myMarker18);
    $(this).addClass('selected');
  }
});

$("#block_t").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker19)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker19);
  } else {
    map.addLayer(myMarker19);
    $(this).addClass('selected');
  }
});

$("#block_v").click(function(event) {
  event.preventDefault();
  if(map.hasLayer(myMarker4)) {
    $(this).removeClass('selected');
    map.removeLayer(myMarker4);
  } else {
    map.addLayer(myMarker4);
    $(this).addClass('selected');
  }
});
