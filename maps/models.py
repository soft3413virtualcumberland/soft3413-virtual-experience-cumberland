# -*- coding: utf-8 -*-
from django.contrib.gis.db import models
from django.db.models import Manager as GeoManager
from django.core.validators import URLValidator
#from future.utils import unicode_literals
import os
from django import template


register = template.Library()


# Create your models here.
class Bathroom(models.Model):
#    building_Description = models.CharField(max_length=200)
    geom = models.PointField(srid=4326,null=True,blank=True)
    objects = models.Manager()
    
# Create your models here.
class Restraurant(models.Model):
    restaurant_Name = models.CharField(max_length=200)
    geom = models.PointField(srid=4326,null=True,blank=True)
    objects = models.Manager()

    
class Building(models.Model):
    building_ID = models.AutoField(primary_key=True)
    building_name = models.CharField(max_length=50, blank=False)
    building_Description = models.TextField(max_length=200)
    building_URL = models.URLField(validators=[URLValidator()], blank=True, null=True,)
    picture = models.ImageField(upload_to='files', max_length=100, blank=True, null=True, default="")
    geom = models.PointField(srid=4326,null=True,blank=True)
    objects = models.Manager()
    
    def picture_url(self):
        if self.picture and hasattr(self.picture, 'url'):
            return self.picture.url
        else:
            return '/static/images/crest3.png'
        
    @register.filter
    def filename(self):
        return os.path.basename(self.picture.name)
