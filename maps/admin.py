from django.contrib import admin
from .models import Building, Bathroom, Restraurant
from leaflet.admin import LeafletGeoAdmin
#
## Register your models here.
#
class BuildingAdmin(LeafletGeoAdmin):
    pass
    list_display = ('building_ID','building_name', 'building_Description', 'building_URL','picture', 'geom', )
    
admin.site.register(Building, BuildingAdmin)  


class BathroomAdmin(LeafletGeoAdmin):
    pass
    list_display = ('geom')
    
admin.site.register(Bathroom, LeafletGeoAdmin) 


#class TransportAdmin(LeafletGeoAdmin):
#    pass
#    list_display = ('geom')
#    
#admin.site.register(TransportStation, LeafletGeoAdmin) 


class RestraurantAdmin(LeafletGeoAdmin):
    pass
    list_display = ('restaurant_Name', 'geom')
    
admin.site.register(Restraurant, RestraurantAdmin) 