from django.shortcuts import render, redirect
from django.views.generic import TemplateView
#from django.shortcuts import redirect
#from django.template import loader
#from django.http import HttpResponse

from django.http import HttpResponse
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
import django.contrib.auth as djauth
import pwgen
import jwt

secret = 'u:!TM&La4h.u2yzBE0SQkDg(EmKwn:2{'

url = 'https://rapid.test.aaf.edu.au/jwt/authnrequest/research/KgtYYMNZ93wFXqCSyROb8A'

config = {}
config['test.aaf.edu.au'] = {
  'iss': 'https://rapid.test.aaf.edu.au',
  'aud': 'https://cumberlandmap.techlab.works/',
}

# Create your views here.
class HomePageView(TemplateView):
    template_name = 'index.html'

class AboutPageView(TemplateView):
    template_name = 'about.html'    
    
class ServicesPageView(TemplateView):
    template_name = 'services.html'
  
class TransportPageView(TemplateView):
    template_name = 'transport.html'
    
    
def googleTransport(request):
    return redirect("https://maps.googleapis.com/maps/api/js?key=AIzaSyA-UkHliC9w77Hq566rxdIggJCwH-kLr0I&callback=initMapTransport")

def login(request):
    return redirect(url)

@csrf_exempt
def auth(request):
    if request.method != 'POST': return HttpResponse('nope')

    try:
        # Verifies signature and expiry time
        verified_jwt = jwt.decode(request.POST['assertion'], secret, audience='https://cumberlandmap.techlab.works/')
        #, options={'verify_aud': False})
        # currently removing audience verification for testing purposes

        # audience error is fixed now! :)

        #print('verified_jwt:', verified_jwt)

        # In a complete app we'd also store and validate the jti value to ensure there is no replay attack
        if verified_jwt['aud'] == config['test.aaf.edu.au']['aud'] and verified_jwt['iss'] == config['test.aaf.edu.au']['iss']:
            request.session['attributes'] = verified_jwt['https://aaf.edu.au/attributes']
            request.session['jwt'] = verified_jwt
            request.session['jws'] = request.POST['assertion']

            good_email = request.session['attributes']['mail']
            firstname = request.session['attributes']['givenname']
            lastname = request.session['attributes']['surname']

            request.session['is_login'] = True
            
            if not User.objects.filter(username=good_email).count():
                u = User.objects.create_user(good_email, good_email, pwgen.pwgen(20, 1, no_symbols=True), first_name=firstname, last_name=lastname)
            else:
                u = User.objects.get(username=good_email)

            # Temporary workaround: http://stackoverflow.com/a/23771930
            u.backend = 'django.contrib.auth.backends.ModelBackend'

            djauth.login(request, u)

            return redirect('home')
        else:
            # Not for this audience
            del request.session['attributes']
            djauth.logout(request)
            raise PermissionDenied
    except jwt.ExpiredSignature:
        # Security cookie has expired
        del request.session['attributes']
        djauth.logout(request)
        raise PermissionDenied

def logout(request):
    del request.session['attributes']
    request.session['is_login'] = False
    djauth.logout(request)
    return redirect('home')
