from django.conf.urls import include,url
from django.views.generic import RedirectView
from .views import HomePageView, AboutPageView, ServicesPageView, TransportPageView, googleTransport
from djgeojson.views import GeoJSONLayerView
from .models import Building, Restraurant, Bathroom
from django.conf import settings
from django.conf.urls.static import static


from django.urls import path
from . import views

urlpatterns = [
     url(r'^$', HomePageView.as_view(template_name = 'index.html'), name = 'home'),
     
#     url(r'^media/$', views.media(model=Building, properties=('picture'), name='media'),

     url(r'^data/$', GeoJSONLayerView.as_view(model=Building), name = 'data'),

     url(r'^houseName/$', GeoJSONLayerView.as_view(model=Building, properties=('building_name', 'building_Description', 'building_URL' )), name = 'houseName'),
     
     url(r'^foodCourts/$', GeoJSONLayerView.as_view(model=Restraurant, properties=('restaurant_Name', 'geom')), name = 'foodCourts'),
     
     url(r'^bathrooms/$', GeoJSONLayerView.as_view(model=Bathroom, properties=('geom')), name = 'bathrooms'),

#     url(r'^stations/$', GeoJSONLayerView.as_view(model=TransportStation, properties=('geom')), name = 'stations'),
     
     url('about/', AboutPageView.as_view(template_name = 'about.html'), name = 'about'),

     url('services/', ServicesPageView.as_view(template_name = 'services.html'), name = 'services'),

     url('transport/', ServicesPageView.as_view(template_name = 'transport.html'), name = 'transport'),
     
     url('googleTransport/', ServicesPageView.as_view(template_name = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA-UkHliC9w77Hq566rxdIggJCwH-kLr0I&callback=initMapTransport'), name = 'googleTransport'),
     
     url('login/', ServicesPageView.as_view(template_name = 'https://rapid.test.aaf.edu.au/jwt/authnrequest/research/KgtYYMNZ93wFXqCSyROb8A'), name = 'login'),
     
     path('auth/jwt', views.auth, name='auth'),

     path('logout', views.logout, name='logout'),
     
     url(r'^favicon\.ico$',RedirectView.as_view(url='/static/images/favicon.png')),
]
##urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



